import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { Transition } from 'react-transition-group';

import logo from './logo.svg';

import './App.css';
import './style.css';

const url = 'https://api.producthunt.com/v1/';

const duration = 500;

const defaultStyle = {
  transition: `opacity ${duration}ms ease-in-out`,
  opacity: 0,
  padding: 20,
  display: 'inline-block',
  border: '1px solid #bdc3c7',
  backgroundColor: '#ffffff',
  boxShadow: 'rgba(0,0,0,.247059)-1px 0 2px 0!important',
  right: 0,
  top: 0,
  height: '100%',
  overflowX: 'auto',
  position: 'fixed',
  zIndex: 10000,
  width: '0',
};

const transitionStyles = {
  entering: { opacity: 0, width: 0 },
  entered: { opacity: 1, width: '700px' },
};

const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'Decemeber'];

class ToggleButton extends Component {
  constructor(props) {
    super(props);
    this.handleToggle = this.handleToggle.bind(this);
  }

  handleToggle() {
    this.props.onOpenButtonClick(true);
  }

  render() {
    return (
      <div className="comments" onClick={() => this.handleToggle()}>
        <span className="icon-comment-fill" />
        <span className="comment-count">
          {this.props.count}
        </span>
      </div>
    );
  }
}

ToggleButton.propTypes = {
  onOpenButtonClick: PropTypes.func,
  count: PropTypes.number
}

class CloseButton extends Component {
  constructor(props) {
    super(props);

    this.handleToggle = this.handleToggle.bind(this);
  }

  handleToggle(e) {
    this.props.onCloseButtonClick(false);
  }

  render() {
    return (
      <div className="close-button" onClick={this.handleToggle} >
        <span className="icon-close-panel" />
      </div>
    );
  }
}

CloseButton.propTypes = {
  onCloseButtonClick: PropTypes.func,
}

class ProductInfo extends Component {
  constructor(props) {
    super(props);

    this.handleOpenButtonClick = this.handleOpenButtonClick.bind(this);
  }

  handleOpenButtonClick(e) {
    this.props.onToggleSidePanel(e);
  }
  render() {
    return (
      <div>
        <div className="upvotes">
          <span className="icon-upvote" /><br />
          {this.props.votes}
        </div>
        <div className="product-info">
          <a href={this.props.redirect_url} target="_blank">
            <h4>
              {this.props.name}
            </h4>
          </a>
          <span className="tagline">
            {this.props.tagline}
          </span><br />
        </div>
        <ToggleButton show={false} onOpenButtonClick={this.handleOpenButtonClick} postdata={this.props} count={this.props.comments} />
      </div>
    );
  }
}

ProductInfo.propTypes = {
  onToggleSidePanel: PropTypes.func,
  votes: PropTypes.number,
  comments: PropTypes.number,
  redirect_url: PropTypes.string,
  taglines: PropTypes.string
}

class Post extends Component {
  constructor(props) {
    super(props);
    this.toggleSidePanel = this.toggleSidePanel.bind(this);
  }
  toggleSidePanel(e) {
    this.props.onToggleSidePanel(e, this.props.postdata.id);
  }
  render() {
    const {
      redirect_url, id, name, tagline, makers, votes_count, comments_count, screenshot_url
    } = this.props.postdata;
    return (
      <div className="product">
        <div className="image thumbnail">
          <a href={redirect_url} target="_blank">
            <img src={screenshot_url['300px']} alt={name} />
          </a>
        </div>
        <ProductInfo
          postid={id}
          name={name}
          tagline={tagline}
          makers={makers}
          votes={votes_count}
          comments={comments_count}
          redirect_url={redirect_url}
          onToggleSidePanel={this.toggleSidePanel}
        />
      </div>
    );
  }
}

Post.propTypes = {
  onToggleSidePanel: PropTypes.func,
  productId: PropTypes.number,
  postdata: PropTypes.object
}

class SidePanel extends Component {
  constructor(props) {
    super(props);

    this.handleCloseButtonClick = this.handleCloseButtonClick.bind(this);
  }
  handleCloseButtonClick(e) {
    this.props.onToggleSidePanel(e);
  }
  render() {
    const {loadingFlag} = this.props;
    if (!loadingFlag) {
      const {
        thumbnail, screenshot_url, topics,
        name, tagline, discussion_url,
        votes_count, redirect_url,
      } = this.props.productData;

      let thumbnailUrl,
        screenshotUrl;

      const topicsList = [];

      if (thumbnail !== 'undefined' && thumbnail !== undefined) {
        thumbnailUrl = thumbnail.image_url;
      }
      if (screenshot_url !== 'undefined' && screenshot_url !== undefined) {
        screenshotUrl = screenshot_url['300px'];
      }
      if (topics !== 'undefined' && topics !== undefined) {
        topics.forEach((topic) => {
          topicsList.push(<span key={topic.name} className="topic">{topic.name}</span>);
        });
      }
      return (
        <div>
          <CloseButton onCloseButtonClick={this.handleCloseButtonClick} />
          <div className="SidePanel">
            <img src={thumbnailUrl} height="80" className="thumbnail" /> <br />
            <h3>
              {name}
            </h3>
            <span className="tagline">
              {tagline}
            </span> <br /> <br />
            {topicsList} <br />
            <div className="get-it">
              <a href={redirect_url} target="_blank">
                GET IT
              </a>
            </div>
            <br />
            <div className="upvote-it">
              <a href={discussion_url} target="_blank">
                <span className="icon-upvote" />
                UPVOTE &nbsp;
                <span className="votes-count">
                  {votes_count}
                </span>
              </a>
            </div><br />
            <img src={screenshotUrl} className="screenshot" />
          </div>
        </div>
      );
    }

    // display loading animation
    return (
      <div>
        <CloseButton onCloseButtonClick={this.handleCloseButtonClick} />
        <div className="SidePanel">
          <div className="spinner">
            <div className="bounce1" />
            <div className="bounce2" />
            <div className="bounce3" />
          </div>
        </div>
      </div>
    );
  }
}

SidePanel.propTypes = {
  onToggleSidePanel: PropTypes.func,
  productData: PropTypes.object
}

class SidePanelContainer extends Component {
  constructor(props) {
    super(props);

    this.toggleSidePanel = this.toggleSidePanel.bind(this);
  }
  toggleSidePanel(e) {
    this.props.onToggleSidePanel(e);
  }
  render() {
    return (
      <Transition in={this.props.in} timeout={duration}>
        {state => (
          <div style={{
            ...defaultStyle,
            ...transitionStyles[state],
          }}
          >
            <SidePanel
              onToggleSidePanel={this.toggleSidePanel}
              productId={this.props.productId}
              productData={this.props.productData}
              show={this.props.show}
              loadingFlag={this.props.loadingFlag}
            />
          </div>
        )}
      </Transition>
    );
  }
}

SidePanelContainer.propTypes = {
  onToggleSidePanel: PropTypes.func,
  in: PropTypes.bool,
  productId: PropTypes.number,
  productData: PropTypes.object,
  loadingFlag: PropTypes.bool,
  show: PropTypes.bool
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      SidePanelState: {
        show: false,
        productId: 'Initial state',
        productData: {},
        loadingFlag: true,
      },
    };

    this.toggleSidePanel = this.toggleSidePanel.bind(this);
  }
  componentDidMount() {
    // Fetch list of top posts
    axios({
      method: 'get',
      url: `${url}posts`,
      responseType: 'json',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer 562d76033a7ba173a5bf97b3b79025bc0977ecf2347547a33a99df49457cca2e ',
        Host: 'api.producthunt.com',
      },
    })
      .then((response) => {
        const {posts} = response.data;
        this.setState({ posts });
      });
  }

  toggleSidePanel(toggle, productId) {
    let SidePanelState = { ...this.state.SidePanelState };
    SidePanelState.show = toggle;
    SidePanelState.loadingFlag = true;
    this.setState({
      SidePanelState,
    });
    if (toggle) {
      // fetch post data
      axios({
        method: 'get',
        url: `${url}posts/${productId}`,
        responseType: 'json',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer 562d76033a7ba173a5bf97b3b79025bc0977ecf2347547a33a99df49457cca2e ',
          Host: 'api.producthunt.com',
        },
      })
        .then((response) => {
          const postData = response.data.post;
          const SidePanelState = { ...this.state.SidePanelState };
          SidePanelState.productId = productId;
          SidePanelState.productData = postData;
          SidePanelState.loadingFlag = false;
          this.setState({
            SidePanelState,
          });
        });
    } else {
    // destroy previous product data
      SidePanelState = { ...this.state.SidePanelState };
      SidePanelState.productData = {};
      SidePanelState.show = toggle;
      this.setState({
        SidePanelState,
      });
    }
  }

  render() {
    const postsArray = this.state.posts;
    const posts = [];
    const {
      show, productId, productData, loadingFlag,
    } = this.state.SidePanelState;
    let overlayToggle,
      target = document.body,
      today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth();
    const month = months[mm];
    if (show) {
      // Display overlay component and disable scroll for body on opening sidepanel
      overlayToggle = { display: 'block' };
      target.style.overflow = 'hidden';
    } else {
      overlayToggle = { display: 'none' };
      target.style.overflow = 'auto';
    }

    postsArray.forEach((post) => {
      posts.push(
        <Post key={post.id}
        postdata={post}
        onToggleSidePanel={this.toggleSidePanel}
        productId={productId} />
      );
    });

    return (
      <div className="App" >
        <div className="brand">
          <img src={logo} alt="logo" className="logo" height="40" width="40" />
          <div className="title"><h1>PH with React</h1></div>
        </div>

        <div className="posts-list">

          <div className="list-header">
            <span className="today">Today </span>
            <span className="date">
              {month} {dd}
            </span>
          </div>

          {posts}
        </div>
        <div className="overlay" style={overlayToggle} />
        <SidePanelContainer
          in={!!show}
          onToggleSidePanel={this.toggleSidePanel}
          productId={productId}
          productData={productData}
          show={show}
          loadingFlag={loadingFlag}
        />
      </div>
    );
  }
}

export default App;
